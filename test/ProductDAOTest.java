/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import stock.management.system.dao.ProductDAO;

/**
 *
 * @author msi06
 */
public class ProductDAOTest {
    
    public ProductDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void countProductsTest() throws SQLException {
     
         ProductDAO product = new ProductDAO();
         
         Assert.assertEquals(5,product.countProducts());
         
     }
     
     @Test
     public void countLowStockProductsTest() throws SQLException {
     
         ProductDAO product = new ProductDAO();
         
         Assert.assertEquals(3,product.countLowStockProducts());
         
     }
}
